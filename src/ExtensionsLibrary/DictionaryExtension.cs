﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ExtensionsLibrary
{
    public static class DictionaryExtension
    {
        public static CastObject CastTo<Tkey, Tvalue, CastObject>(this IDictionary<Tkey, Tvalue> dictionary) where CastObject : class
        {
            CastObject obj = null;
            if (dictionary.IsNotNullAndEmpty())
            {
                var json = JsonConvert.SerializeObject(dictionary);
                obj = JsonConvert.DeserializeObject<CastObject>(json);
            }
            return obj;
        }
    }
}
