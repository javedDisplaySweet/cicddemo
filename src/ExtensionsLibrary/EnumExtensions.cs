using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace ExtensionsLibrary
{
    public static class EnumExtensions
    {
        public static string GetDescription<T>(this T GenericEnum) where T : IConvertible
        {
            if (GenericEnum is Enum)
            {
                Type genericEnumType = GenericEnum.GetType();
                MemberInfo[] memberInfo = genericEnumType.GetMember(GenericEnum.ToString());
                if ((memberInfo != null && memberInfo.Length > 0))
                {
                    var _Attribs = memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
                    if ((_Attribs != null && _Attribs.Count() > 0))
                    {
                        return ((System.ComponentModel.DescriptionAttribute)_Attribs.ElementAt(0)).Description;
                    }
                }
                return GenericEnum.ToString();
            }
            return null; // could also return string.Empty
        }
    }
}