﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExtensionsLibrary
{
    public static class CollectionExtensions
    {
        public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> self) => self.Select((item, index) => (item, index));
        public static void AddRangeIfNotNullAndEmpty<T>(this ICollection<T> sourceCollection, ICollection<T> collectionToAdd)
        {
            if (collectionToAdd.IsNotNullAndEmpty())
            {
                foreach (var item in collectionToAdd) 
                {
                    sourceCollection.Add(item);
                }
            }
        }
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> sourceCollection)
        {
            var isNullOrEmpty = sourceCollection == null || sourceCollection.Count() == 0;
            return isNullOrEmpty;
        }
        public static bool IsNotNullAndEmpty<T>(this IEnumerable<T> sourceCollection)
        {
            var isNotNullOrEmpty = sourceCollection != null && sourceCollection.Count() > 0;
            return isNotNullOrEmpty;
        }
        public static IEnumerable<TResult> LeftOuterJoin<TLeft, TRight, TKey, TResult>(this IEnumerable<TLeft> leftEnumerable, IEnumerable<TRight> rightEnumerable, Func<TLeft, TKey> leftKey, Func<TRight, TKey> rightKey, Func<TLeft, TRight, TResult> result)
        {
            if (leftEnumerable == null)
            {
                return new List<TResult>();
            }
            if (rightEnumerable == null)
            {
                rightEnumerable = new List<TRight>();
            }
            return from leftItem in leftEnumerable
                   join rightItem in rightEnumerable on leftKey.Invoke(leftItem) equals rightKey.Invoke(rightItem) into joinedOutput
                   from rightOutputItem in joinedOutput.DefaultIfEmpty()
                   select result.Invoke(leftItem, rightOutputItem);
        }
        public static IEnumerable<TResult> RightOuterJoin<TLeft, TRight, TKey, TResult>(this IEnumerable<TLeft> leftEnumerable, IEnumerable<TRight> rightEnumerable, Func<TLeft, TKey> leftKey, Func<TRight, TKey> rightKey, Func<TLeft, TRight, TResult> result)
        {
            if (rightEnumerable == null)
            {
                return new List<TResult>();
            }
            if (leftEnumerable == null)
            {
                leftEnumerable = new List<TLeft>();
            }
            return from rightItem in rightEnumerable
                   join leftItem in leftEnumerable on rightKey.Invoke(rightItem) equals leftKey.Invoke(leftItem) into joinedOutput
                   from leftOutputItem in joinedOutput.DefaultIfEmpty()
                   select result.Invoke(leftOutputItem, rightItem);
        }
        public static IEnumerable<TResult> FullOuterJoin<TLeft, TRight, TKey, TResult>(this IEnumerable<TLeft> leftEnumerable, IEnumerable<TRight> rightEnumerable, Func<TLeft, TKey> leftKey, Func<TRight, TKey> rightKey, Func<TLeft, TRight, TResult> result)
        {
            var leftOuterJoinOutput = LeftOuterJoin(leftEnumerable, rightEnumerable, leftKey, rightKey, result);
            var rightOuterJoinOutput = RightOuterJoin(leftEnumerable, rightEnumerable, leftKey, rightKey, result);

            var fullOuterJoin = leftOuterJoinOutput.Union(rightOuterJoinOutput);
            return fullOuterJoin;
        }
    }
}