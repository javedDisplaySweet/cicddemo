using System;

namespace ExtensionsLibrary
{
    public static class DateTimeExtensions
    {
        public static string DatetimeToString(this DateTime datetime)
        {
            // 2018-03-28T06:00:00.000Z
            return datetime.ToString("yyyy-MM-ddTHH:mm:ss") + ".000Z";
        }
    }
}