#addin nuget:?package=Cake.Coverlet
#tool nuget:?package=ReportGenerator

var target = Argument("target", "Report-Generation");
var configuration = Argument("configuration", "Release");
////////////////////////////////
// TO do : Move user and password to environment variable
////////////////////////////////////////////////////////
 var coverageDirectory = Directory("./code_coverage");
 var reportsDirectory = coverageDirectory + Directory("reports");
 var cuberturaFileName = "results";
 var cuberturaFileExtension = ".cobertura.xml";
 var reportTypes = "Html"; // Use "Html" value locally for performance and files' size.
 var coverageFilePath = coverageDirectory + File(cuberturaFileName + cuberturaFileExtension);
 var jsonFilePath = coverageDirectory + File(cuberturaFileName + ".json");

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////
Task("Clean")
    .Does(() =>
    {
        CleanDirectory($"./bin/{configuration}");
        CleanDirectory($"./artifacts/");
    });
   

Task("Restore")
    .Does(()=>
   {
        DotNetCoreRestore("./DemoSolution.sln", new DotNetCoreRestoreSettings
       {
             Sources = new[] {"https://api.nuget.org/v3/index.json"},
             Verbosity = DotNetCoreVerbosity.Normal
        });
   });

Task("Build")
    .IsDependentOn("Clean")
    .IsDependentOn("Restore")
    .Does(()=>
    {
        DotNetCoreBuild("./DemoSolution.sln", new DotNetCoreBuildSettings
        {
            Configuration = configuration,
             NoRestore = true
        });
    });

Task("Test")
  .IsDependentOn("Build")
  .Does(()=>
  {
    var success = true;
    var coverletSettings = new CoverletSettings
    {
        CollectCoverage = true,
        CoverletOutputDirectory = coverageDirectory,
        CoverletOutputName = cuberturaFileName,
        MergeWithFile = jsonFilePath,
        CoverletOutputFormat  = CoverletOutputFormat.cobertura,
    };
    
      var dotNetTestSettings = new DotNetCoreTestSettings
      {
            Configuration = configuration,
            NoBuild = true,
            NoRestore =true
      };
            GetFiles("./tests/**/*.csproj")
            .ToList()
            .ForEach(project => {
                    try 
                    {
                      
                        CleanDirectory(reportsDirectory);
                        EnsureDirectoryExists(reportsDirectory);
                        Information($"reports directory {reportsDirectory}");
                        DotNetCoreTest(project.FullPath, dotNetTestSettings, coverletSettings);
                    }
                    catch(Exception ex)
                    {
                        success = false;
                        Error("There was an error while running the tests", ex);
                    }
            });
            
    });

Task("Report-Generation")
  .IsDependentOn("Test")
  .Does(()=>
  {
           var isGitLabCIBuild = GitLabCI.IsRunningOnGitLabCI;
            Information($"Running on gitlab ci : {isGitLabCIBuild}");
              var reportSettings = new ReportGeneratorSettings
            {
                ArgumentCustomization = args => args.Append($"-reportTypes:Html")
            };
            if(isGitLabCIBuild)
            {
                var toolpath = Context.Tools.Resolve("net5.0/ReportGenerator.dll");
                Information($"Tool PAth : {toolpath.ToString()}");
                 reportSettings.ToolPath = toolpath;
            }
            ReportGenerator(coverageFilePath, reportsDirectory, reportSettings);
  });
//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
